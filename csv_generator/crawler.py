import requests
import math
import threading
import json


BASE_URL = "https://www.olx.co.id/api/relevance/search?"
MAX_PAGE = 50
MIN_PRICE = 200 # 200 * 1000
MAX_PRICE = 90000000 # 90M * 1000
TOTAL_THREAD = 1
PRICE_RANGE = (MAX_PRICE - MIN_PRICE) // TOTAL_THREAD
threads = [None] * TOTAL_THREAD


def create_param(page=0, **filter):
	return dict(
		category=198,
		location=1000001,
		page=page,
		**filter
	)


def get_data(params):
	try:
		r = requests.get(BASE_URL, params=params, timeout=10)
		print(r.request.url)

		return r.json()
	except Exception as e:
		print("Error timeout. Redoing it again..")

		return None


def get_data_pages(total_page, **filter):
	data = []
	page, count = 0, 0

	while (page < total_page):
		resp = get_data(create_param(page=page, **filter))

		if resp is None or type(resp) != dict or "data" not in resp:
			continue 

		resp_data = resp["data"]
		if len(resp_data) <= 0 and count < 5:
			print("False data size. Redoing the data..")
			count += 1
		else:
			data += resp["data"]
			page += 1
			count = 0

	return data


def get_data_from_range_price(price_min, price_max, index):
	print("Running from thread {}".format(index))
	current_price = price_min
	total = 0

	while current_price < price_max:
		price_threshold = price_max

		resp = get_data(create_param(price_min=current_price*1000, price_max=price_threshold*1000))
		total_page = resp["metadata"].get("total_pages", 0)
		print(current_price*1000, price_threshold*1000, total_page)

		if total_page > 50:
			while (current_price < price_threshold):
				mid = (current_price + price_threshold + 1) // 2
				resp = get_data(create_param(price_min=current_price*1000, price_max=mid*1000))

				if resp is None or type(resp) != dict or "metadata" not in resp:
					continue 

				total_page = resp["metadata"].get("total_pages", 0)
				price_threshold = mid - 1

				if (total_page <= 50):
					break

			print(current_price*1000, price_threshold*1000, total_page)

			if (total_page > 50):
				total_page = 50

		print("Thread: {} -> Price Range: {} - {} | Total Page: {}".format(index, current_price * 1000, price_threshold * 1000, total_page))
		if total_page > 0:
			data = get_data_pages(total_page, price_min=current_price*1000, price_max=price_threshold*1000)

			print("Thread: {} -> Saving data to data-{}-{}.json".format(index, index, total))
			with open("data/data-{}-{}.json".format(index, total), "w") as f:
				f.write(json.dumps(data))

			total += 1

		current_price = price_threshold + 1
		

for i in range(len(threads)):
	start_price = MIN_PRICE + (i * PRICE_RANGE)
	end_price = start_price + PRICE_RANGE - 1
	threads[i] = threading.Thread(target=get_data_from_range_price, args=(start_price, end_price, i))
	threads[i].start()
	print("Starting thread {} for price: {} - {}".format(i, start_price * 1000, end_price * 1000))


for i in range(len(threads)):
    threads[i].join()


print("Done")